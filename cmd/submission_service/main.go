package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	APIInternal "submission-service/internal/api"
	loggerInternal "submission-service/internal/logger"
	postgresInternal "submission-service/internal/postgres"
	probeInternal "submission-service/internal/probe"
	rabbitmqInternal "submission-service/internal/rabbitmq"
	redisInternal "submission-service/internal/redis"
	resultsInternal "submission-service/internal/results"
	specificationInternal "submission-service/internal/specification"
	"submission-service/pkg/logger"
)

func main() {
	var err error

	if err = loggerInternal.Initialize(); err != nil {
		panic(err)
	}
	defer logger.Logger.Sync()

	if err = probeInternal.Initialize("probe"); err != nil {
		panic(err)
	}
	defer probeInternal.Cancel()

	if err = specificationInternal.Initialize("app"); err != nil {
		panic(err)
	}

	if err = APIInternal.InitializePrivate("private_api"); err != nil {
		panic(err)
	}

	if err = APIInternal.InitializePublic("public_api"); err != nil {
		panic(err)
	}

	if err = postgresInternal.Initialize("postgres"); err != nil {
		panic(err)
	}

	if err = redisInternal.Initialize("redis"); err != nil {
		panic(err)
	}

	if err = rabbitmqInternal.Initialize("rabbitmq"); err != nil {
		panic(err)
	}

	if err = resultsInternal.Initialize("results"); err != nil {
		panic(err)
	}

	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
	<-c
	logger.Sugar.Info("Termination signal received. Starting graceful shutdown.")

	ctx, cancel := context.WithTimeout(context.Background(), specificationInternal.Specification.GracefulShutdownTimeout)
	defer cancel()

	APIInternal.PublicAPI.Server.Shutdown(ctx)
	APIInternal.PrivateAPI.Server.Shutdown(ctx)

	resultsInternal.GracefulShutdown()
	rabbitmqInternal.CloseConnections()

	logger.Sugar.Info("Shutting down.")
}
