# Submission Service

TODO:
 - [ ] versioning
 - [X] prober
 - [ ] try to reconnect when probes are failing
 - [X] postgres
 - [ ] pgx logger for queries
 - [ ] kafka
 - [ ] redis
 - [ ] status 508
 - [ ] WithMinimumLatency
 - [ ] liveness readiness for k8s
 - [ ] profiling
 - [X] sort tasks in search results


## Build
```
make docker
```

## Deploy
```
make deploy
```

## Expose port 8080 (once)
```
make expose
```

## Lint example
```
golangci-lint run internal/task --enable-all --disable wsl --disable nlreturn
```

