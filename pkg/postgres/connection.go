package postgres

import (
	"database/sql"
	"fmt"

	_ "github.com/jackc/pgx/v4/stdlib"
)

func NewDatabaseConnection(spec *Specification) (*sql.DB, error) {
	// TODO: add zapadapter.NewLogger(logger.Logger),

	db, err := sql.Open(
		"pgx",
		fmt.Sprintf(
			"user=%s password=%s host=%s port=%d",
			spec.User, spec.Password, spec.Host, spec.Port, // FIXME(torilov): add database
		),
	)
	if err != nil {
		return nil, err
	}

	return db, err
}
