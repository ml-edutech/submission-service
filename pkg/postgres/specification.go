package postgres

import "github.com/kelseyhightower/envconfig"

type Specification struct {
	Host     string `required:"true"`
	Port     int    `required:"true"`
	User     string `required:"true"`
	Password string `required:"true"`
	Database string `required:"true"`
	SSLMode  string `required:"true"`

	envName string
}

func NewSpecification(envName string) (*Specification, error) {
	spec := Specification{envName: envName}
	if err := spec.Parse(envName); err != nil {
		return nil, err
	}

	return &spec, nil
}

func (s *Specification) Parse(envName string) error {
	return envconfig.Process(envName, s)
}
