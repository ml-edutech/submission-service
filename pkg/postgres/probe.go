package postgres

import (
	"context"
	"database/sql"
)

type Probe struct {
	DB *sql.DB
}

func NewProbe(db *sql.DB) (*Probe, error) {
	return &Probe{DB: db}, nil
}

func (p *Probe) ProbeContext(ctx context.Context) error {
	err := p.DB.PingContext(ctx)

	return err
}
