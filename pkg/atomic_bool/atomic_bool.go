package atomicbool

import (
	"sync/atomic"
)

type AtomicBool struct {
	flag *int32
}

func (b *AtomicBool) Get() bool {
	return atomic.LoadInt32(b.flag) == 1
}

func (b *AtomicBool) SetFromBool(f bool) {
	if f {
		atomic.StoreInt32(b.flag, 1)
	} else {
		atomic.StoreInt32(b.flag, 0)
	}
}

func (b *AtomicBool) SetFromAtomicBool(f *AtomicBool) {
	atomic.StoreInt32(b.flag, *f.flag)
}

func NewAtomicBool(f bool) (*AtomicBool, error) {
	flag := new(int32)
	if f {
		*flag = 1
	} else {
		*flag = 0
	}

	return &AtomicBool{flag: flag}, nil
}
