package specification

type Specification interface {
	Parse(envName string) error
}
