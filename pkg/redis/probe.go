package redis

import (
	"context"
)

type Probe struct {
	DB *Client
}

func NewProbe(db *Client) (*Probe, error) {
	return &Probe{DB: db}, nil
}

func (p *Probe) ProbeContext(ctx context.Context) error {
	_, err := p.DB.Ping(ctx).Result()

	return err
}
