package redis

import (
	"fmt"

	"github.com/kelseyhightower/envconfig"
)

type Specification struct {
	Host     string `required:"true"`
	Port     int    `default:"6379"`
	Password string `default:""`
	DB       int    `default:"0"`

	envName string
}

func NewSpecification(envName string) (*Specification, error) {
	spec := Specification{envName: envName}
	if err := spec.Parse(envName); err != nil {
		return nil, err
	}

	return &spec, nil
}

func (s *Specification) Parse(envName string) error {
	return envconfig.Process(envName, s)
}

// GetAddr returns address in "host:port" notation.
func (s *Specification) GetAddr() string {
	return fmt.Sprintf("%s:%d", s.Host, s.Port)
}
