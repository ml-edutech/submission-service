package redis

import (
	"github.com/go-redis/redis/v8"
)

// Client is a configurable wrapper for go-redis Client.
type Client struct {
	// Embedding of the redis.Client
	redis.Client
}

// NewClient constructs new Client struct from spec.Specification and checks ping.
func NewClient(spec *Specification) (*Client, error) {
	client := Client{*redis.NewClient(&redis.Options{
		Addr:     spec.GetAddr(),
		Password: spec.Password,
		DB:       spec.DB,
	})}

	return &client, nil
}
