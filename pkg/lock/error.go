package lock

// LocksError is a custom error type for failed TryLock.
type LocksError struct {
	What string
}

// Error implements error interface.
func (e *LocksError) Error() string {
	return e.What
}

var (
	errorTryLock          = &LocksError{What: "try lock failed"}
	errorTryUnlock        = &LocksError{What: "try unlock failed"}
	errorTryDropWrongUUID = &LocksError{What: "wrong uuid to drop"}
)
