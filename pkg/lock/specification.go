package lock

import (
	"time"

	"github.com/kelseyhightower/envconfig"
)

type Specification struct {
	AcquireDuration    time.Duration `envconfig:"LOCK_ACQUIRE_DURATION" default:"512s" required:"true"`
	TryLockDuration    time.Duration `default:"100ms" required:"true"`
	TryLockRetryTick   time.Duration `default:"7ms" required:"true"`
	TryUnlockDuration  time.Duration `default:"100ms" required:"true"`
	TryUnlockRetryTick time.Duration `default:"7ms" required:"true"`

	envName string
}

func NewSpecification(envName string) (*Specification, error) {
	spec := Specification{envName: envName}
	if err := spec.Parse(envName); err != nil {
		return nil, err
	}

	return &spec, nil
}

func (s *Specification) Parse(envName string) error {
	return envconfig.Process(envName, s)
}
