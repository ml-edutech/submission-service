package lock

import (
	"context"
	"errors"
	"time"

	redisClient "submission-service/pkg/redis"

	"github.com/go-redis/redis/v8"
	uuid "github.com/google/uuid"
)

// Locker is a struct that performs locks.
type Locker struct {
	// AcquireDuration describes for how long the lock is acquired.
	AcquireDuration time.Duration

	// TryLockDuration describes the time to try to lock.
	TryLockDuration time.Duration
	// TryLockRetryTick describes the duration between ticks when trying to lock.
	TryLockRetryTick time.Duration

	// TryUnockDuration describes the time to try to unlock.
	TryUnlockDuration time.Duration
	// TryUnockRetryTick describes the duration between ticks when trying to unlock.
	TryUnlockRetryTick time.Duration

	// RedisClientDB is a go-redis Client wrapper.
	RedisClientDB *redisClient.Client
}

// NewLocker constructs a new Locker instance.
func NewLocker(cfg Specification, rcl redisClient.Client) (*Locker, error) {
	return &Locker{
		AcquireDuration:    cfg.AcquireDuration,
		TryLockDuration:    cfg.TryLockDuration,
		TryLockRetryTick:   cfg.TryLockRetryTick,
		TryUnlockDuration:  cfg.TryUnlockDuration,
		TryUnlockRetryTick: cfg.TryUnlockRetryTick,
		RedisClientDB:      &rcl,
	}, nil
}

// AcquireWithContext acquires lock lockName.
func (l *Locker) AcquireWithContext(ctx context.Context, lockName string) (*uuid.UUID, error) {
	id, err := uuid.NewRandom()
	if err != nil {
		return nil, err // TODO(torilov): wrap errors from external packages
	}

	ticker := time.NewTicker(l.TryLockRetryTick)

	for {
		select {
		case <-ctx.Done():
			return nil, errorTryLock
		case <-ticker.C:
			cmd := l.RedisClientDB.SetNX(ctx, lockName, id.String(), l.AcquireDuration)
			if cmd.Err() == nil && cmd.Val() {
				return &id, nil
			}
		}
	}
}

// Acquire acquires lock lockName.
func (l *Locker) Acquire(lockName string) (*uuid.UUID, error) {
	ctx, cancel := context.WithTimeout(context.Background(), l.TryLockDuration)
	defer cancel()

	return l.AcquireWithContext(ctx, lockName)
}

// ReleaseWithContext releases the lock lockName.
func (l *Locker) ReleaseWithContext(ctx context.Context, lockName string, id uuid.UUID) error {
	value := (id).String()

	// Transactional function.
	txf := func(tx *redis.Tx) error {
		redisID, err := tx.Get(ctx, lockName).Result()

		if err != nil || errors.Is(err, redis.Nil) {
			return err // TODO(torilov): wrap errors from external packages
		}

		if redisID != value {
			return errorTryDropWrongUUID
		}

		_, err = tx.TxPipelined(ctx, func(pipe redis.Pipeliner) error {
			pipe.Del(ctx, lockName)

			return nil
		})

		return err // TODO(torilov): wrap errors from external packages
	}

	ticker := time.NewTicker(l.TryUnlockRetryTick)

	for {
		select {
		case <-ctx.Done():
			return errorTryUnlock
		case <-ticker.C:
			err := l.RedisClientDB.Watch(ctx, txf, lockName)
			if err == nil {
				return nil
			}

			if errors.Is(err, redis.TxFailedErr) {
				continue
			}

			return err // TODO(torilov): wrap errors from external packages
		}
	}
}

// Release releases lock lockName.
func (l *Locker) Release(lockName string, id uuid.UUID) error {
	ctx, cancel := context.WithTimeout(context.Background(), l.TryUnlockDuration)
	defer cancel()

	return l.ReleaseWithContext(ctx, lockName, id)
}
