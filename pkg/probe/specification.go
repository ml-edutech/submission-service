package probe

import (
	"time"

	"github.com/kelseyhightower/envconfig"
)

type Specification struct {
	// Interval between checks.
	Interval time.Duration `required:"true"`
	// Timeout for each check.
	Timeout time.Duration `required:"true"`

	envName string
}

func NewSpecification(envName string) (*Specification, error) {
	spec := Specification{envName: envName}
	if err := spec.Parse(envName); err != nil {
		return nil, err
	}

	return &spec, nil
}

func (s *Specification) Parse(envName string) error {
	return envconfig.Process(envName, s)
}
