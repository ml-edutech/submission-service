package probe

import (
	atomicBool "submission-service/pkg/atomic_bool"
)

// AppState stores such information as liveness or readiness.
type AppState struct {
	Value *atomicBool.AtomicBool
}

func NewAppStateProbe(value bool) (*AppState, error) {
	var err error

	var b *atomicBool.AtomicBool

	if b, err = atomicBool.NewAtomicBool(value); err != nil {
		return nil, err
	}

	probe := &AppState{Value: b}

	return probe, nil
}

func (p *AppState) SetFromBool(value bool) {
	p.Value.SetFromBool(value)
}

func (p *AppState) SetFromAtomicBool(value *atomicBool.AtomicBool) {
	p.Value.SetFromAtomicBool(value)
}

func (p *AppState) Get() bool {
	return p.Value.Get()
}
