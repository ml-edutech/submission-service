package probe

import (
	"context"
	"sync"
	"time"

	atomicBool "submission-service/pkg/atomic_bool"
	"submission-service/pkg/logger"
)

// Manager drives app's liveness and readiness through running permanent probes of the services the app uses.
type Manager struct {
	Liveness  *AppState
	Readiness *AppState

	probes   map[string]Probe
	probesMx *sync.RWMutex

	probesRestartTimeout time.Duration
	probesFailTimeout    time.Duration
}

// NewManager returns new probe manager with liveness set to true and readiness set to false.
func NewManager(spec *Specification) (*Manager, error) {
	var liveness *AppState

	var readiness *AppState

	var err error

	if liveness, err = NewAppStateProbe(true); err != nil {
		return nil, err
	}

	if readiness, err = NewAppStateProbe(false); err != nil {
		return nil, err
	}

	return &Manager{
		Liveness:  liveness,
		Readiness: readiness,

		probes:   map[string]Probe{},
		probesMx: &sync.RWMutex{},

		probesRestartTimeout: spec.Interval,
		probesFailTimeout:    spec.Timeout,
	}, nil
}

func (m *Manager) InsertProbe(key string, probe Probe) {
	m.probesMx.Lock()
	defer m.probesMx.Unlock()
	m.probes[key] = probe
}

func (m *Manager) DeleteProbe(key string) {
	m.probesMx.Lock()
	defer m.probesMx.Unlock()
	delete(m.probes, key)
}

func (m *Manager) RunProbesPermanentlyContext(ctx context.Context) {
	ticker := time.NewTicker(m.probesRestartTimeout)

	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			m.RunProbesOnceContext(ctx)
		}
	}
}

func (m *Manager) RunProbesOnceContext(ctx context.Context) {
	m.probesMx.RLock()
	defer m.probesMx.RUnlock()

	ready, _ := atomicBool.NewAtomicBool(true)

	var wg sync.WaitGroup

	for key, p := range m.probes {
		wg.Add(1)

		go func(key string, probe Probe) {
			defer wg.Done()

			timeout, cancel := context.WithTimeout(ctx, m.probesFailTimeout)
			defer cancel()

			if err := probe.ProbeContext(timeout); err != nil {
				logger.Sugar.Errorf("Error on probe %s.", key)
				ready.SetFromBool(false)
			}
		}(key, p)
	}

	wg.Wait()

	m.Readiness.SetFromAtomicBool(ready)
}
