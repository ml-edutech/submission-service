package probe

import (
	"context"
)

type Probe interface {
	ProbeContext(ctx context.Context) error
}
