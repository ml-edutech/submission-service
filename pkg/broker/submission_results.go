package broker

type SubmissionResult struct {
	ID                      string        `json:"submission_id"`
	UserID                  string        `json:"user_id"`
	Status                  string        `json:"status"`
	PublicTests             *PublicTests  `json:"public_tests"`
	PrivateTests            *PrivateTests `json:"private_tests"`
	AcquiredLockCertificate string        `json:"acquired_lock_certificate"`
}

type PublicTests struct {
	ReturnCode           string `json:"return_code"`
	TestingOutput        string `json:"testing_output"`
	TotalTestsNumber     int    `json:"total_tests_number"`
	FirstErrorTestNumber int    `json:"first_error_test_number"`
}

type PrivateTests struct {
	ReturnCode           string `json:"return_code"`
	TotalTestsNumber     int    `json:"total_tests_number"`
	FirstErrorTestNumber int    `json:"first_error_test_number"`
}
