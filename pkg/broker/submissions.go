package broker

type Submission struct {
	ID                      string `json:"submission_id"`
	UserID                  string `json:"user_id"`
	UserCode                string `json:"user_code"`
	PublicTests             string `json:"public_tests"`
	PrivateTests            string `json:"private_tests"`
	AcquiredLockCertificate string `json:"acquired_lock_certificate"`
}
