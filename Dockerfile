FROM golang:1.15-alpine AS build
WORKDIR /src
ENV CGO_ENABLED=0
ENV GO111MODULE=on
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
ENV GOOS=linux
ENV GOARCH=amd64
RUN go build cmd/submission_service/main.go


FROM alpine:3.13 AS bin
COPY --from=build /src/main /
CMD ./main
