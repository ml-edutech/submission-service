.PHONY: run build docker deploy expose follow-logs

run:
	go run cmd/submission_service/main.go

build:
	go build cmd/submission_service/main.go

docker:
	docker build -t ml-edutech/submission-service:latest .

deploy:
	kubectl delete deployment submission-service || echo "new deployment"
	kustomize build deployments/k8s-spec/submission-service | kubectl apply -f -

expose:
	kubectl expose deployment submission-service --type=LoadBalancer --port 8080

