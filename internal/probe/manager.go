package probeinternal

import (
	"context"

	"submission-service/pkg/logger"
	"submission-service/pkg/probe"
)

var (
	Specification *probe.Specification
	Manager       *probe.Manager
	Context       context.Context
	Cancel        func()
)

func Initialize(envName string) error {
	var err error

	logger.Sugar.Info("Initialize Probes.")

	if Specification, err = probe.NewSpecification(envName); err != nil {
		return err
	}

	if Manager, err = probe.NewManager(Specification); err != nil {
		return err
	}

	Context, Cancel = context.WithCancel(context.Background())

	logger.Sugar.Info("Start Probes.")

	go Manager.RunProbesPermanentlyContext(Context)

	return nil
}
