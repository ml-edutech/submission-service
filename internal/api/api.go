package apiinternal

import (
	privateAPI "submission-service/internal/private_api"
	publicApi "submission-service/internal/public_api"
	"submission-service/pkg/api"
	"submission-service/pkg/logger"
)

var (
	PrivateSpecification *api.Specification
	PublicSpecification  *api.Specification
	PrivateAPI           *api.API
	PublicAPI            *api.API
)

func InitializePrivate(envName string) error {
	var err error

	logger.Sugar.Info("Parse Private API Specification.")

	if PrivateSpecification, err = api.NewSpecification(envName); err != nil {
		return err
	}

	if PrivateAPI, err = api.NewAPI(PrivateSpecification, privateAPI.GetHandler()); err != nil {
		return err
	}

	logger.Sugar.Infof("Start Private API on %s.", PrivateSpecification.GetAddr())

	go PrivateAPI.Run()

	return nil
}

func InitializePublic(envName string) error {
	var err error

	logger.Sugar.Info("Parse Public API Specification.")

	if PublicSpecification, err = api.NewSpecification(envName); err != nil {
		return err
	}

	if PublicAPI, err = api.NewAPI(PublicSpecification, publicApi.GetHandler()); err != nil {
		return err
	}

	logger.Sugar.Infof("Start Public API on %s.", PublicSpecification.GetAddr())

	go PublicAPI.Run()

	return nil
}
