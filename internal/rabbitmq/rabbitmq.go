package rabbitmqinternal

import (
	"submission-service/pkg/logger"

	"github.com/streadway/amqp"
)

var (
	SpecificationInternal *Specification
	Client                *amqp.Connection
	Channel               *amqp.Channel
	WriteQueue            amqp.Queue
	ReadQueue             amqp.Queue
	ReadMessages          <-chan amqp.Delivery
)

func Initialize(envName string) error {
	// TODO(torilov): add probe
	var err error

	logger.Sugar.Info("Parse RabbitMQ Specification.")

	if SpecificationInternal, err = NewSpecification(envName); err != nil {
		return err
	}

	if Client, err = amqp.Dial(SpecificationInternal.URL); err != nil {
		return err
	}

	if Channel, err = Client.Channel(); err != nil {
		return err
	}

	if ReadQueue, err = Channel.QueueDeclare(SpecificationInternal.ReadQueue, true, false,
		false, false, nil); err != nil {
		return err
	}

	if err = Channel.Qos(SpecificationInternal.ReadQueuePrefetchCount, 0, false); err != nil {
		return err
	}

	if WriteQueue, err = Channel.QueueDeclare(SpecificationInternal.WriteQueue, true, false,
		false, false, nil); err != nil {
		return err
	}

	if ReadMessages, err = Channel.Consume(ReadQueue.Name, "", false, false,
		false, false, nil); err != nil {
		return err
	}

	return nil
}

func CloseConnections() {
	Channel.Close()
	Client.Close()
}
