package rabbitmqinternal

import (
	"github.com/kelseyhightower/envconfig"
)

type Specification struct {
	URL string `required:"true"`

	ReadQueue              string `envconfig:"READ_QUEUE" required:"true"`
	ReadQueuePrefetchCount int    `envconfig:"READ_QUEUE_PREFETCH_COUNT" required:"true"`

	WriteQueue string `envconfig:"WRITE_QUEUE" required:"true"`

	envName string
}

func NewSpecification(envName string) (*Specification, error) {
	spec := Specification{envName: envName}
	if err := spec.Parse(envName); err != nil {
		return nil, err
	}

	return &spec, nil
}

func (s *Specification) Parse(envName string) error {
	return envconfig.Process(envName, s)
}
