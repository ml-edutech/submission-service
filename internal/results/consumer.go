package resultsInternal

import (
	"context"

	"submission-service/pkg/logger"
)

var (
	Spec          Specification
	ReplicaNumber int
	Ctx           context.Context
	Cancel        context.CancelFunc
	RunStopped    chan struct{}
)

func Initialize(envName string) error {
	logger.Sugar.Info("Parse Operator Specification.")

	var err error
	var Spec *Specification

	if Spec, err = NewSpecification(envName); err != nil {
		return err
	}

	ReplicaNumber = Spec.ReplicaNumber

	Ctx, Cancel = context.WithCancel(context.Background())

	RunStopped = make(chan struct{})

	go Run(Ctx)

	return nil
}

func GracefulShutdown() {
	Cancel()
	<-RunStopped
}
