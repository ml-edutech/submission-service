package resultsInternal

import (
	"github.com/kelseyhightower/envconfig"
)

type Specification struct {
	ReplicaNumber int `envconfig:"ROUTINES_NUMBER" required:"true"`

	envName string
}

func NewSpecification(envName string) (*Specification, error) {
	spec := Specification{envName: envName}
	if err := spec.Parse(envName); err != nil {
		return nil, err
	}

	return &spec, nil
}

func (s *Specification) Parse(envName string) error {
	return envconfig.Process(envName, s)
}
