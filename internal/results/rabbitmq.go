package resultsInternal

import (
	"context"
	"sync"

	"github.com/streadway/amqp"

	rabbitmqInternal "submission-service/internal/rabbitmq"
	"submission-service/pkg/logger"
)

func Run(ctx context.Context) {
	sem := make(chan struct{}, ReplicaNumber)
	for i := 0; i < ReplicaNumber; i++ {
		sem <- struct{}{}
	}

	var wg sync.WaitGroup

	for {
		select {
		case <-ctx.Done():
			goto shutdown
		case <-sem:
			wg.Add(1)
			go ProcessSingleMessage(ctx, sem, &wg)
		}
	}

shutdown:
	wg.Wait()
	RunStopped <- struct{}{}
}

func ProcessSingleMessage(ctx context.Context, sem chan<- struct{}, wg *sync.WaitGroup) {
	var d amqp.Delivery
	var ok bool
	var err error

	select {
	case <-ctx.Done():
		goto shutdown
	case d, ok = <-rabbitmqInternal.ReadMessages:
		if !ok {
			goto shutdown
		}
	}

	if err = Launch(ctx, d); err != nil {
		logger.Sugar.Errorf("Error while parsing submussion result. Error: %v.", err)
		goto shutdown
	}

shutdown:
	wg.Done()
	sem <- struct{}{}
}
