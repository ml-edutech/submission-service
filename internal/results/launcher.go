package resultsInternal

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/streadway/amqp"

	postgresInternal "submission-service/internal/postgres"
	redisInternal "submission-service/internal/redis"
	"submission-service/pkg/broker"
	"submission-service/pkg/lock"
	"submission-service/pkg/logger"
)

func Launch(ctx context.Context, d amqp.Delivery) error {
	var submissionResult broker.SubmissionResult
	var err error
	var publicTestsId string
	var privateTestsId string

	spec, err := lock.NewSpecification("locker_submissions")
	if err != nil {
		logger.Sugar.Errorf("Error during locker specification initialization. Error: %s.", err.Error())
		return err
	}

	locker, err := lock.NewLocker(*spec, *redisInternal.DB)
	if err != nil {
		logger.Sugar.Errorf("Error during locker initialization. Error: %s.", err.Error())
		return err
	}

	if err = json.Unmarshal(d.Body, &submissionResult); err != nil {
		return fmt.Errorf("unmarshal body %s submissionResult. error: %w", string(d.Body), err)
	}

	lockCert, err := uuid.Parse(submissionResult.AcquiredLockCertificate)
	if err != nil {
		return fmt.Errorf("parse acquired certificate %s. error: %w", submissionResult.AcquiredLockCertificate, err)
	}

	logger.Sugar.Infof("User %s released lock with certificate %s", submissionResult.UserID, lockCert.String())
	if err = locker.Release(submissionResult.UserID, lockCert); err != nil {
		logger.Sugar.Errorf("User %s failed to released lock with certificate %s", submissionResult.UserID, lockCert.String())
	}

	logger.Sugar.Infof("Procesing submission result %submissionResult.", submissionResult.ID)

	if submissionResult.PublicTests != nil {
		err = postgresInternal.DB.QueryRowContext(ctx,
			`INSERT INTO submissions_tests_public (return_code, testing_output, total_tests_number, first_error_test_number) VALUES ($1, $2, $3, $4) RETURNING id`,
			submissionResult.PublicTests.ReturnCode,
			submissionResult.PublicTests.TestingOutput,
			submissionResult.PublicTests.TotalTestsNumber,
			submissionResult.PublicTests.FirstErrorTestNumber).Scan(&publicTestsId)

		if err != nil {
			logger.Sugar.Errorf("Error running insert public tests submission result query in. Error: %v", err.Error())
			return err
		}
	}

	if submissionResult.PrivateTests != nil {
		err = postgresInternal.DB.QueryRowContext(ctx,
			`INSERT INTO submissions_tests_private (return_code, total_tests_number, first_error_test_number) VALUES ($1, $2, $3) RETURNING id`,
			submissionResult.PrivateTests.ReturnCode,
			submissionResult.PrivateTests.TotalTestsNumber,
			submissionResult.PrivateTests.FirstErrorTestNumber).Scan(&privateTestsId)

		if err != nil {
			logger.Sugar.Errorf("Error running insert private tests submission result query in. Error: %v", err.Error())
			return err
		}
	}

	row := postgresInternal.DB.QueryRowContext(ctx,
		`UPDATE submissions SET status=$1, public_tests_id=$2, private_tests_id=$3 WHERE submissions.id::text=$4`,
		submissionResult.Status, publicTestsId, privateTestsId, submissionResult.ID)

	if row.Err() != nil {
		logger.Sugar.Errorf("Error running insert submission query in. Error: %v", err.Error())
		return err
	}

	d.Ack(false)
	return nil
}
