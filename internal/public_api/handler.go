package apipublic

import (
	"submission-service/internal/submission"
	"submission-service/internal/task"
	"submission-service/pkg/logger"

	"github.com/gorilla/mux"
)

func GetHandler() *mux.Router {
	router := mux.NewRouter()

	router.HandleFunc("/tasks", task.GetTasks).
		Methods("GET")

	router.HandleFunc("/tasks/{task_id}", task.GetTaskByID).
		Methods("GET")

	router.HandleFunc("/submissions", submission.GetSubmissions).
		Methods("GET")

	router.HandleFunc("/submissions/{submission_id}", submission.GetSubmissionByID).
		Methods("GET")

	router.HandleFunc("/submissions", submission.CreateSubmission).
		Methods("POST")

	router.Use(logger.LoggingMiddleware)

	return router
}
