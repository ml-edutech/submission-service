package apiprivate

import (
	"submission-service/pkg/logger"

	"github.com/gorilla/mux"
)

func GetHandler() *mux.Router {
	router := mux.NewRouter()

	router.HandleFunc("/liveness", livenessProbeHandler)
	router.HandleFunc("/readiness", readinessProbeHandler)

	router.Use(logger.LoggingMiddleware)

	return router
}
