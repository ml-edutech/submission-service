package apiprivate

import (
	"net/http"

	internalProbeManager "submission-service/internal/probe"
)

func livenessProbeHandler(w http.ResponseWriter, r *http.Request) {
	if internalProbeManager.Manager.Liveness.Value.Get() {
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusServiceUnavailable)
	}
}

func readinessProbeHandler(w http.ResponseWriter, r *http.Request) {
	if internalProbeManager.Manager.Readiness.Value.Get() {
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusServiceUnavailable)
	}
}
