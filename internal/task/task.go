package task

import (
	"encoding/json"

	"github.com/jackc/pgtype"
)

type UserTask struct {
	ID           pgtype.UUID
	Number       pgtype.Int4
	Name         pgtype.Varchar
	Level        pgtype.Varchar
	Description  pgtype.Text
	CodeTemplate pgtype.Text
	Hints        pgtype.TextArray
	Tags         pgtype.TextArray

	Status pgtype.Varchar
}

func (t UserTask) MarshalJSON() ([]byte, error) {
	hints := make([]string, 0)
	tags := make([]string, 0)

	for _, value := range t.Hints.Elements {
		hints = append(hints, value.String)
	}

	for _, value := range t.Tags.Elements {
		tags = append(tags, value.String)
	}

	return json.Marshal(&struct {
		ID           pgtype.UUID `json:"id"`
		Number       int32       `json:"number"`
		Name         string      `json:"name"`
		Level        string      `json:"level"`
		Description  string      `json:"description"`
		CodeTemplate string      `json:"code_template"`
		Hints        []string    `json:"hints"`
		Tags         []string    `json:"tags"`

		Status string `json:"status"`
	}{
		ID:           t.ID,
		Number:       t.Number.Int,
		Name:         t.Name.String,
		Level:        t.Level.String,
		Description:  t.Description.String,
		CodeTemplate: t.CodeTemplate.String,
		Hints:        hints,
		Tags:         tags,

		Status: t.Status.String,
	})
}

type UserTaskNoDetails struct {
	ID                   pgtype.UUID
	Number               pgtype.Int4
	Name                 pgtype.Varchar
	Level                pgtype.Varchar
	Tags                 pgtype.TextArray
	ExpectedSolutionTime pgtype.Varchar // TODO: migrate to pgtype.Interval
	Author               pgtype.Varchar

	Status pgtype.Varchar
}

func (t UserTaskNoDetails) MarshalJSON() ([]byte, error) {
	tags := make([]string, 0)

	for _, value := range t.Tags.Elements {
		tags = append(tags, value.String)
	}

	return json.Marshal(&struct {
		ID                   pgtype.UUID `json:"id"`
		Number               int32       `json:"number"`
		Name                 string      `json:"name"`
		Level                string      `json:"level"`
		Tags                 []string    `json:"tags"`
		ExpectedSolutionTime string      `json:"expected_solution_time"`
		Author               string      `json:"author"`

		Status string `json:"status"`
	}{
		ID:                   t.ID,
		Number:               t.Number.Int,
		Name:                 t.Name.String,
		Level:                t.Level.String,
		Tags:                 tags,
		ExpectedSolutionTime: t.ExpectedSolutionTime.String,
		Author:               t.Author.String,

		Status: t.Status.String,
	})
}
