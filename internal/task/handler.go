package task

import (
	"database/sql"
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"strings"

	postgresInternal "submission-service/internal/postgres"
	"submission-service/pkg/logger"

	"github.com/gorilla/mux"
)

func GetTasks(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	var err error

	// Request parameters.
	number := -1
	if q := r.FormValue("number"); q != "" {
		if number, err = strconv.Atoi(q); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
	}

	level := "any"
	if q := r.FormValue("level"); q != "" {
		switch {
		case strings.EqualFold(q, "easy"):
			level = "easy"
		case strings.EqualFold(q, "medium"):
			level = "medium"
		case strings.EqualFold(q, "hard"):
			level = "hard"
		default:
			w.WriteHeader(http.StatusBadRequest)
			return
		}
	}

	status := "any"
	if q := r.FormValue("status"); q != "" {
		switch {
		case strings.EqualFold(q, "not_attempted"):
			status = "not_attempted"
		case strings.EqualFold(q, "in_progress"):
			status = "in_progress"
		case strings.EqualFold(q, "done"):
			status = "done"
		default:
			w.WriteHeader(http.StatusBadRequest)
			return
		}
	}

	text := r.FormValue("text")

	limit := 20
	if q := r.FormValue("limit"); q != "" {
		if limit, err = strconv.Atoi(q); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
	}

	offset := 0
	if q := r.FormValue("offset"); q != "" {
		if offset, err = strconv.Atoi(q); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
	}

	var rows *sql.Rows

	// TODO(torilov): add tags parameter

	if userID := r.Header.Get("X-User"); userID != "" {
		// Authorized
		rows, err = postgresInternal.DB.QueryContext(r.Context(),
			`SELECT tasks.id, tasks.number_, tasks.name, tasks.level, tasks.tags, task_statuses.status, tasks.expected_solution_time, tasks.author
				FROM tasks JOIN task_statuses ON tasks.id=task_statuses.task_id
				WHERE $1=task_statuses.user_id and ($2=-1 or $2=tasks.number_)
				  and ($3='any' or $3::task_level=tasks.level) and ($4='any' or $4::task_status=task_statuses.status)
				  and ($5='' or (tasks.name ~* $5 or tasks.description ~* $5))
				ORDER BY number_
				LIMIT $6 OFFSET $7;`,
			userID, number, level, status, text, limit, offset,
		)
	} else {
		// Not Authorized
		rows, err = postgresInternal.DB.QueryContext(r.Context(),
			`SELECT id, number_, name, level, tags, 'not_attempted', expected_solution_time, author
				FROM tasks
				WHERE ($1=-1 or $1=number_) and ($2='any' or $2::task_level=level) and ($3='' or (name ~* $3 or description ~* $3))
				ORDER BY number_
				LIMIT $4 OFFSET $5;`,
			number, level, text, limit, offset,
		)
	}

	if err != nil {
		logger.Sugar.Errorf("Error running select query in search tasks. Error: %v", err.Error())
		w.WriteHeader(http.StatusServiceUnavailable)

		return
	}
	defer rows.Close()

	tasks := make([]UserTaskNoDetails, 0)

	for rows.Next() {
		var task UserTaskNoDetails
		if err := rows.Scan(&task.ID, &task.Number, &task.Name, &task.Level, &task.Tags, &task.Status, &task.ExpectedSolutionTime, &task.Author); err != nil {
			logger.Sugar.Errorf("Error during selecting tasks. Error: %s.", err.Error())
			w.WriteHeader(http.StatusServiceUnavailable)
			return
		}
		tasks = append(tasks, task)
	}

	var body []byte

	if body, err = json.Marshal(tasks); err != nil {
		logger.Sugar.Errorf("Error during json marshaling. Error: %s.", err.Error())
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	if _, err := w.Write(body); err != nil {
		logger.Sugar.Errorf("Error during body write. Error: %s.", err.Error())
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
}

func GetTaskByID(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	var err error

	taskID := mux.Vars(r)["task_id"]

	var task UserTask

	if userID := r.Header.Get("X-User"); userID != "" {
		// Authorized
		err = postgresInternal.DB.QueryRowContext(r.Context(),
			`SELECT tasks.id, tasks.number_, tasks.name, tasks.level, tasks.description,
				tasks.code_template, tasks.hints, tasks.tags, task_statuses.status
				FROM tasks JOIN task_statuses ON tasks.id=task_statuses.task_id
				WHERE $1=tasks.id and $2=task_statuses.user_id;`,
			taskID, userID,
		).
			Scan(&task.ID, &task.Number, &task.Name, &task.Level, &task.Description,
				&task.CodeTemplate, &task.Hints, &task.Tags, &task.Status)
	} else {
		// Not Authorized
		err = postgresInternal.DB.QueryRowContext(r.Context(),
			`SELECT id, number_, name, level, description, code_template, hints, tags, 'not_attempted'
				FROM tasks
				WHERE id = $1;`,
			taskID,
		).
			Scan(&task.ID, &task.Number, &task.Name, &task.Level, &task.Description,
				&task.CodeTemplate, &task.Hints, &task.Tags, &task.Status)
	}

	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			logger.Sugar.Infof("Requested task with ID that not exists: %s.", taskID)
			w.WriteHeader(http.StatusNoContent)
			return
		}

		logger.Sugar.Errorf("Error during selecting task by id. ID: %s. Error: %s.", taskID, err.Error())
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	var body []byte

	if body, err = json.Marshal(task); err != nil {
		logger.Sugar.Errorf("Error during json marshaling. Error: %s.", err.Error())
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	if _, err := w.Write(body); err != nil {
		logger.Sugar.Errorf("Error during body write. Error: %s.", err.Error())
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
}
