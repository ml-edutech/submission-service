package postgresintenral

import (
	"database/sql"

	internalProbeManager "submission-service/internal/probe"
	"submission-service/pkg/logger"
	"submission-service/pkg/postgres"
)

var (
	Specification *postgres.Specification
	DB            *sql.DB
)

func Initialize(envName string) error {
	var err error

	logger.Sugar.Info("Parse Postgres Specification.")

	if Specification, err = postgres.NewSpecification(envName); err != nil {
		return err
	}

	if DB, err = postgres.NewDatabaseConnection(Specification); err != nil {
		return err
	}

	logger.Sugar.Infof("Adding probe for Postgres on %s:%d.", Specification.Host, Specification.Port)

	internalProbeManager.Manager.InsertProbe(envName, &postgres.Probe{DB: DB})

	return nil
}
