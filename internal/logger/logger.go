package loggerinternal

import (
	"submission-service/pkg/logger"

	"go.uber.org/zap"
)

func Initialize() error {
	var err error

	if logger.Logger, err = zap.NewProduction(); err != nil {
		return err
	}

	logger.Sugar = logger.Logger.Sugar()

	return nil
}
