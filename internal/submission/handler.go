package submission

import (
	"database/sql"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	redisInternal "submission-service/internal/redis"
	"submission-service/pkg/broker"
	"submission-service/pkg/lock"
	"submission-service/pkg/logger"

	"github.com/gorilla/mux"
	"github.com/streadway/amqp"

	postgresInternal "submission-service/internal/postgres"
	rabbitmqInternal "submission-service/internal/rabbitmq"
)

func CreateSubmission(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	userID := r.Header.Get("X-User")
	if userID == "" {
		logger.Sugar.Info("Create submission called with empty userID.")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	var err error
	var rawBody []byte
	if rawBody, err = ioutil.ReadAll(r.Body); err != nil {
		logger.Sugar.Errorf("Error while reading request body. Error: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var submission UserPostSubmission
	if err = json.Unmarshal(rawBody, &submission); err != nil {
		logger.Sugar.Errorf("Error while unmarshaling request json. Error: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	logger.Sugar.Infof("Creating Submission for task %s from User %s", submission.TaskID, userID)

	spec, err := lock.NewSpecification("locker_submissions")
	if err != nil {
		logger.Sugar.Errorf("Error during locker specification initialization. Error: %s.", err.Error())
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	locker, err := lock.NewLocker(*spec, *redisInternal.DB)
	if err != nil {
		logger.Sugar.Errorf("Error during locker initialization. Error: %s.", err.Error())
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	lockCert, err := locker.AcquireWithContext(r.Context(), userID)
	if err != nil {
		logger.Sugar.Errorf("Error during acquiring lock for user %s. Error: %s.", userID, err.Error())
		w.WriteHeader(http.StatusLoopDetected)
		return
	}

	logger.Sugar.Infof("User %s acquired lock with certificate %s", userID, lockCert.String())

	var submissionID string
	err = postgresInternal.DB.QueryRowContext(r.Context(),
		`INSERT INTO submissions (task_id, user_id, submitted_at, user_code, status) VALUES ($1, $2, $3, $4, 'waiting') RETURNING id`,
		submission.TaskID, userID, time.Now(), submission.UserCode).Scan(&submissionID)

	if err != nil {
		logger.Sugar.Errorf("Error running insert submission query in. Error: %v", err.Error())
		w.WriteHeader(http.StatusServiceUnavailable)

		logger.Sugar.Infof("User %s released lock with certificate %s", userID, lockCert.String())
		if err = locker.Release(userID, *lockCert); err != nil {
			logger.Sugar.Errorf("User %s failed to released lock with certificate %s", userID, lockCert.String())
		}

		return
	}

	message := broker.Submission{
		ID:                      submissionID,
		UserID:                  userID,
		UserCode:                submission.UserCode,
		AcquiredLockCertificate: lockCert.String(),
	}

	err = postgresInternal.DB.QueryRowContext(r.Context(),
		`SELECT public_tests, private_tests FROM tasks WHERE tasks.id::text=$1`,
		submission.TaskID,
	).Scan(&message.PublicTests, &message.PrivateTests)

	if err != nil {
		logger.Sugar.Errorf("Error running select query to get tests. Error: %v", err.Error())
		w.WriteHeader(http.StatusServiceUnavailable)

		logger.Sugar.Infof("User %s released lock with certificate %s", userID, lockCert.String())
		locker.Release(userID, *lockCert)

		return
	}

	messageBody, err := json.Marshal(message)
	if err != nil {
		logger.Sugar.Errorf("Error while marshaling message %v. Error: %v", message, err)
		w.WriteHeader(http.StatusServiceUnavailable)

		logger.Sugar.Infof("User %s released lock with certificate %s", userID, lockCert.String())
		locker.Release(userID, *lockCert)

		return
	}

	if err = rabbitmqInternal.Channel.Publish("", rabbitmqInternal.WriteQueue.Name, false, false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "application/json",
			Body:         messageBody,
		}); err != nil {
		logger.Sugar.Errorf("Error while sending message to rabbitmq. Error: %v", err)
		w.WriteHeader(http.StatusServiceUnavailable)

		logger.Sugar.Infof("User %s released lock with certificate %s", userID, lockCert.String())
		locker.Release(userID, *lockCert)

		return
	}
}

func GetSubmissionByID(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	var err error

	userID := r.Header.Get("X-User")
	if userID == "" {
		logger.Sugar.Info("Get submission called with empty userID.")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	submissionID := mux.Vars(r)["submission_id"]

	submission := UserSubmission{
		PublicTests:  new(UserSubmissionPublicTestStatus),
		PrivateTests: new(UserSubmissionPrivateTestStatus),
	}

	err = postgresInternal.DB.QueryRowContext(r.Context(),
		`SELECT submissions.id, submissions.task_id, submissions.submitted_at, submissions.tested_at, submissions.status, submissions.user_code, 
				public_tests.return_code, public_tests.testing_output, public_tests.total_tests_number, public_tests.first_error_test_number,
				private_tests.return_code, private_tests.total_tests_number, private_tests.first_error_test_number
				FROM submissions
				LEFT JOIN submissions_tests_public public_tests ON submissions.public_tests_id = public_tests.id
				LEFT JOIN submissions_tests_private private_tests ON submissions.private_tests_id = private_tests.id
				WHERE submissions.user_id=$1 and submissions.id=$2;`,
		userID, submissionID,
	).Scan(&submission.ID, &submission.TaskID, &submission.SubmittedAt, &submission.TestedAt, &submission.Status, &submission.UserCode,
		&submission.PublicTests.ReturnCode, &submission.PublicTests.TestingOutput, &submission.PublicTests.TotalTestsNumber, &submission.PublicTests.FirstFailedTestNumber,
		&submission.PrivateTests.ReturnCode, &submission.PrivateTests.TotalTestsNumber, &submission.PrivateTests.FirstFailedTestNumber,
	)

	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			logger.Sugar.Errorf("Nothing found for submission id=%v and user id=%v", submissionID, userID)
			w.WriteHeader(http.StatusNoContent)

			return
		}
		logger.Sugar.Errorf("Error running select query in search submissions. Error: %v", err.Error())
		w.WriteHeader(http.StatusServiceUnavailable)

		return
	}

	var body []byte

	if body, err = json.Marshal(submission); err != nil {
		logger.Sugar.Errorf("Error during json marshaling. Error: %s.", err.Error())
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	if _, err := w.Write(body); err != nil {
		logger.Sugar.Errorf("Error during body write. Error: %s.", err.Error())
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
}

func GetSubmissions(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	var err error

	userID := r.Header.Get("X-User")
	if userID == "" {
		logger.Sugar.Info("Create submission called with empty userID.")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	taskID := r.FormValue("task_id")

	limit := 20
	if q := r.FormValue("limit"); q != "" {
		if limit, err = strconv.Atoi(q); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
	}

	offset := 0
	if q := r.FormValue("offset"); q != "" {
		if offset, err = strconv.Atoi(q); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
	}

	var rows *sql.Rows
	rows, err = postgresInternal.DB.QueryContext(r.Context(),
		`SELECT id, task_id, submitted_at, tested_at, status
				FROM submissions
				WHERE user_id=$1 and task_id::text ~* $2
				ORDER BY submitted_at
				LIMIT $3 OFFSET $4`,
		userID, taskID, limit, offset,
	)

	if err != nil {
		logger.Sugar.Errorf("Error running select query in search submissions. Error: %v", err.Error())
		w.WriteHeader(http.StatusServiceUnavailable)

		return
	}
	defer rows.Close()

	submissions := make([]UserSubmissionNoDetails, 0)

	for rows.Next() {
		var submission UserSubmissionNoDetails
		if err := rows.Scan(&submission.ID, &submission.TaskID, &submission.SubmittedAt, &submission.TestedAt, &submission.Status); err != nil {
			logger.Sugar.Errorf("Error during selecting submissions. Error: %s.", err.Error())
			w.WriteHeader(http.StatusServiceUnavailable)
			return
		}
		submissions = append(submissions, submission)
	}

	var body []byte

	if body, err = json.Marshal(submissions); err != nil {
		logger.Sugar.Errorf("Error during json marshaling. Error: %s.", err.Error())
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	if _, err := w.Write(body); err != nil {
		logger.Sugar.Errorf("Error during body write. Error: %s.", err.Error())
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
}
