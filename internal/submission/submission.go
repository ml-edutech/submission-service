package submission

import (
	"encoding/json"

	"github.com/jackc/pgtype"
)

type UserPostSubmission struct {
	TaskID   string `json:"task_id"`
	UserCode string `json:"user_code"`
}

type UserSubmissionNoDetails struct {
	ID          pgtype.UUID        `json:"id"`
	TaskID      pgtype.UUID        `json:"task_id"`
	SubmittedAt pgtype.Timestamptz `json:"submitted_at"`
	TestedAt    pgtype.Timestamptz `json:"tested_at"`
	Status      pgtype.Text        `json:"status"` // TODO: check if Enum usable here
}

type UserSubmissionPublicTestStatus struct {
	ReturnCode            pgtype.Varchar
	TestingOutput         pgtype.Text
	TotalTestsNumber      pgtype.Int4
	FirstFailedTestNumber pgtype.Int4
}

type UserSubmissionPrivateTestStatus struct {
	ReturnCode            pgtype.Varchar
	TotalTestsNumber      pgtype.Int4
	FirstFailedTestNumber pgtype.Int4
}

type UserSubmission struct {
	ID           pgtype.UUID                      `json:"id"`
	TaskID       pgtype.UUID                      `json:"task_id"`
	SubmittedAt  pgtype.Timestamptz               `json:"submitted_at"`
	TestedAt     pgtype.Timestamptz               `json:"tested_at"`
	Status       pgtype.Text                      `json:"status"` // TODO: check if Enum usable here
	UserCode     pgtype.Text                      `json:"user_code"`
	PublicTests  *UserSubmissionPublicTestStatus  `json:"public_tests"`
	PrivateTests *UserSubmissionPrivateTestStatus `json:"private_tests"`
}

func (t UserSubmissionPrivateTestStatus) MarshalJSON() ([]byte, error) {
	if t.ReturnCode.Status == pgtype.Null &&
		t.TotalTestsNumber.Status == pgtype.Null &&
		t.FirstFailedTestNumber.Status == pgtype.Null {
		return json.Marshal(nil)
	}

	if t.ReturnCode.String == "ok" {
		return json.Marshal(&struct {
			ReturnCode       string `json:"return_code"`
			TotalTestsNumber int32  `json:"total_tests_number"`
		}{
			ReturnCode:       t.ReturnCode.String,
			TotalTestsNumber: t.TotalTestsNumber.Int,
		})
	}

	return json.Marshal(&struct {
		ReturnCode            string `json:"return_code"`
		TotalTestsNumber      int32  `json:"total_tests_number"`
		FirstFailedTestNumber int32  `json:"first_failed_test_number"`
	}{
		ReturnCode:            t.ReturnCode.String,
		TotalTestsNumber:      t.TotalTestsNumber.Int,
		FirstFailedTestNumber: t.FirstFailedTestNumber.Int,
	})
}

func (t UserSubmissionPublicTestStatus) MarshalJSON() ([]byte, error) {
	if t.ReturnCode.Status == pgtype.Null &&
		t.TestingOutput.Status == pgtype.Null &&
		t.TotalTestsNumber.Status == pgtype.Null &&
		t.FirstFailedTestNumber.Status == pgtype.Null {
		return json.Marshal(nil)
	}

	if t.ReturnCode.String == "ok" {
		return json.Marshal(&struct {
			ReturnCode       string `json:"return_code"`
			TestingOutput    string `json:"testing_output"`
			TotalTestsNumber int32  `json:"total_tests_number"`
		}{
			TestingOutput:    t.TestingOutput.String,
			ReturnCode:       t.ReturnCode.String,
			TotalTestsNumber: t.TotalTestsNumber.Int,
		})
	}

	return json.Marshal(&struct {
		ReturnCode            string `json:"return_code"`
		TestingOutput         string `json:"testing_output"`
		TotalTestsNumber      int32  `json:"total_tests_number"`
		FirstFailedTestNumber int32  `json:"first_failed_test_number"`
	}{
		ReturnCode:            t.ReturnCode.String,
		TestingOutput:         t.TestingOutput.String,
		TotalTestsNumber:      t.TotalTestsNumber.Int,
		FirstFailedTestNumber: t.FirstFailedTestNumber.Int,
	})
}
