package redisinternal

import (
	internalProbeManager "submission-service/internal/probe"
	"submission-service/pkg/logger"
	"submission-service/pkg/redis"
)

var (
	Specification *redis.Specification
	DB            *redis.Client
)

func Initialize(envName string) error {
	var err error

	logger.Sugar.Info("Parse Redis Specification.")

	if Specification, err = redis.NewSpecification(envName); err != nil {
		return err
	}

	if DB, err = redis.NewClient(Specification); err != nil {
		return err
	}

	logger.Sugar.Infof("Adding probe for Redis on %s:%d.", Specification.Host, Specification.Port)

	internalProbeManager.Manager.InsertProbe(envName, &redis.Probe{DB: DB})

	return nil
}
